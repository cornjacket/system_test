package app

import (
	"fmt"
	"log"
	"testing"
	"time"

	. "gopkg.in/check.v1"

	cmd_client "bitbucket.org/cornjacket/cmd_hndlr/client_lib"
	view_client "bitbucket.org/cornjacket/view_hndlr/client_lib"
	"bitbucket.org/cornjacket/iot/message"
	//"bitbucket.org/cornjacket/iot/serverlib/tallocrespproc" // DRT removed for testing
)

var pktCmdClient cmd_client.PacketCmdHndlrService
var tRespCmdClient cmd_client.TallocRespCmdHndlrService
var viewClient view_client.ViewHndlrService

// Hook up gocheck into the "go test" runner.
func Test(t *testing.T) { TestingT(t) }

type TestSuite struct {
}

var _ = Suite(&TestSuite{})

func (s *TestSuite) SetUpSuite(c *C) {
	fmt.Println("SetUpSuite() invoked")
	for i := 1; i <= 15; i++ {
		// TODO(drt): Add a hook so that this wait is not required for local 'go test'
		fmt.Println("Initial wait before Servers are up. This is needed for circleci flow.")
		time.Sleep(1 * time.Second)
	}
	

	// time delay to guarantee so that the server is up and talking to the database
	time.Sleep(1 * time.Second)
	// Initialize Packet Client
	if err := pktCmdClient.Open("localhost", "8080", "/packet"); err != nil {
		log.Fatal("pktCmdClient.Open() error: ", err)
	}
	// Initialize TallocResp Client
	if err := tRespCmdClient.Open("localhost", "8080", "/tallocresp"); err != nil {
		log.Fatal("tRespCmdClient.Open() error: ", err)
	}
	// Initialize ViewHndlrService Client
	config := view_client.ViewHndlrServiceConfig{
        	TransportType: view_client.HttpPost,
        	Hostname:      "localhost",
        	Port:          "8082",
        	PacketPath:    "/packet",
        	NodestatePath: "/nodestate",
        	DataPath:      "/data",
		}
	if err := viewClient.Open(config); err != nil {
		log.Fatal("viewClient.Open() error: ", err)
	}

}

func (s *TestSuite) SetUpTest(c *C) {
	fmt.Println("SetupTest() invoked")
}

func (s *TestSuite) TearDownTest(c *C) {
	fmt.Println("TearDownTest() invoked")
	// TODO(drt) - I could add a test interface to drop/add tables to clear the previous test data
}

func (s *TestSuite) TearDownSuite(c *C) {
	fmt.Println("TearDownSuite() invoked")

	// TODO(drt) - close the db connection

	/* Why is this here?
	for i := 1; i <= 15; i++ {
		// TODO(drt): Add a hook so that this wait is not required for local 'go test'
		fmt.Println("Initial wait before Servers are up. This is needed for circleci flow.")
		time.Sleep(1 * time.Second)
	}
	*/
}


func (s *TestSuite) TestRxAppStatusPacket123aPath(c *C) {
	// Send Packet
	fmt.Printf("component_test.packetTest\n")

	p := message.UpPacket{
		Dr:   "SF10 BW125 4/5",
		Ts:   uint64(1529525468005),
		Eui:  "000000000000123a",
		Ack:  false,
		Cmd:  "rx",
		Snr:  13.3,
		Data: "0a03000228ffff23ea58b9",
		Fcnt: 1115,
		Freq: uint64(90230000),
		Port: 1,
		Rssi: -88,
		}
	err := pktCmdClient.SendPacket(p)
	c.Assert(err, Equals, nil)

	time.Sleep(1 * time.Second) // Delay so that the packet can traverse the microservice pipeline 

	pktReq := view_client.PacketReq{
        	TsStart: uint64(0),
        	TsEnd:   uint64(1000),
        	Eui:     "000000000000123a",
		}

	pktResp, err := viewClient.GetPackets(pktReq)
	c.Assert(err, Equals, nil)
	pktsLength := len(pktResp.Packets)
	c.Assert(pktsLength, Equals, 1)
	if err == nil && pktsLength == 1 {
		c.Assert(pktResp.Eui, Equals, p.Eui)
		c.Assert(pktResp.Packets[0].Data, Equals, p.Data)
	}

	// TODO(DRT) - There is a consistency problem with the ValueA and B being saved in the data table, but not in the nodestate.
	// Since there is no valid onboard, there should not be any data being stored in either. The nodestate is correct but the data store is not.
	dataReq := view_client.DataStateReq{
        	TsStart: uint64(0),
        	TsEnd:   uint64(1000),
        	Eui:     "000000000000123a",
		}
	dataResp, err := viewClient.GetData(dataReq)
	c.Assert(err, Equals, nil)
	dataLength := len(dataResp.Data)
	c.Assert(dataLength, Equals, 1)
	if err == nil && dataLength == 1 {
		c.Assert(dataResp.Eui, Equals, p.Eui)
		c.Assert(dataResp.Data[0].ValueA, Equals, 1)
		c.Assert(dataResp.Data[0].ValueB, Equals, 1)
	}

/*
	nodeResp, err := viewClient.GetNodeState(p.Eui)
	c.Assert(err, Equals, nil)
	if err == nil {
		c.Assert(nodeResp.Eui, Equals, p.Eui)
		c.Assert(nodeResp.ActualStateA, Equals, 1)
		c.Assert(nodeResp.ActualStateB, Equals, 1)
	}
*/

}

/*

// TODO: Not sure if this test belongs here since it involves interaction with ATC. Perhaps this is really just a component test.
func (s *TestSuite) TestTallocRespPath(c *C) {
	// Send Packet
	fmt.Printf("component_test.tallocrespTest\n")

	r := tallocrespproc.TallocResp{
		NodeEui: "1234",
		Tslot:   4,
		Code:    1, // >0 will be valid, otherwise error code
		Test:    false,
		Ts:      uint64(0),
		CorrId:  uint64(1),
		ClassId: "4.3.6",
		Hops:    1,
	}

	err := tRespCmdClient.SendTallocResp(r)
	c.Assert(err, Equals, nil)
	time.Sleep(100 * time.Millisecond) // This is needed so that the TestTearDown func doesn't drop the tables before the event_hndlr is finished processing

	//c.Assert(mocks.EventLastTallocResp.NodeEui, Equals, r.NodeEui)
	//c.Assert(mocks.EventLastTallocResp.Tslot, Equals, r.Tslot)
	//c.Assert(mocks.EventLastTallocResp.Code, Equals, r.Code)

}

// TODO(DRT) - I need to enable ATC before I can run this test. But maybe this test should be part of the ATC test suite and not here.
func (s *TestSuite) TestGwPacketPath(c *C) {
	// Send Packet
	fmt.Printf("component_test.packetTest\n")

	p := message.UpPacket{
		Dr:   "?",
		Ts:   uint64(12345),
		Eui:  "1234",
		Ack:  false,
		Cmd:  "gw",
		Snr:  12.4,
		Data: "12345678", // this is a random packet that should get rejected by the controller
		Fcnt: 12,
		Freq: uint64(20),
		Port: 1,
		Rssi: -110,
	}
	err := pktCmdClient.SendPacket(p)
	c.Assert(err, Equals, nil)
	time.Sleep(100 * time.Millisecond) // This is needed so that the TestTearDown func doesn't drop the tables before the event_hndlr is finished processing

	//c.Assert(mocks.AtcLastPacket.Data, Equals, p.Data)
	//c.Assert(mocks.AtcLastPacket.Eui, Equals, p.Eui)
	//c.Assert(mocks.AtcLastPacket.Cmd, Equals, p.Cmd)

}
*/
