mod_replace:
	#go mod edit -replace bitbucket.org/cornjacket/cmd_hndlr/app=/home/david/work/go/src/bitbucket.org/cornjacket/cmd_hndlr/app
	#go mod edit -replace bitbucket.org/cornjacket/event_hndlr/client=/home/david/work/go/src/bitbucket.org/cornjacket/event_hndlr/client
	#go mod edit -replace bitbucket.org/cornjacket/iot=/home/david/work/go/src/bitbucket.org/cornjacket/iot
mod_drop_replace:
	#go mod edit -dropreplace bitbucket.org/cornjacket/cmd_hndlr/app
	#go mod edit -dropreplace bitbucket.org/cornjacket/event_hndlr/client
	#go mod edit -dropreplace bitbucket.org/cornjacket/iot
docker_local_bash:
	docker exec -it local_cmd_hndlr /bin/ash
docker_run_prod:
	docker run -it --detach --name=local_cmd_hndlr -p 8080:8080 cornjacket/iot_app_4_cmd_hndlr
docker_run_dev:
	docker run -it --name=local_cmd_hndlr -p 8080:8080 cmd_hndlr_dev 
docker_build_prod:
	docker build . -f ./docker/Dockerfile.prod -t cornjacket/iot_app_4_cmd_hndlr
docker_build_dev:
	docker build . -f ./docker/Dockerfile.dev -t cmd_hndlr_dev
kafka_up:
	docker-compose -f ./local_test/kafka/docker-compose.yaml up
kafka_down:
	docker-compose -f ./local_test/kafka/docker-compose.yaml down
