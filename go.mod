module bitbucket.org/cornjacket/system_test

go 1.13

require (
	bitbucket.org/cornjacket/cmd_hndlr v0.1.5
	bitbucket.org/cornjacket/cmd_hndlr/app v0.1.5 // indirect
	bitbucket.org/cornjacket/iot v0.1.7
	bitbucket.org/cornjacket/view_hndlr v0.1.5
	gopkg.in/check.v1 v1.0.0-20200902074654-038fdea0a05b
)
